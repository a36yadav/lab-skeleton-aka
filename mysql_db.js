require("dotenv").config();
const util = require("util");
const uuid = require("uuid");
const mysql = require("mysql2-promise")();
const logger = require("./logger");
let requests = 0;

const init = async () => {
	mysql.configure({
		host: process.env.RDS_HOSTNAME,
		user: process.env.RDS_USERNAME,
		password: process.env.RDS_PASSWORD,
		port: process.env.RDS_PORT,
		database: process.env.RDS_DATABASE,
	});
	//  try {
	//    await connect();
	//    console.log("Database connected!");
	//  } catch (e) {
	//    console.error(e);
	//  }
};

const queryProductById = async (productId) => {
	requests += 1;
	logger.info("GET request received at /product/:productId, parameters: " + JSON.stringify(productId))
	logger.info(requests + " requests received so far")
	return (await mysql.query(`SELECT *
                              FROM products
                              WHERE id = "${productId}";`))[0][0];
};

const queryRandomProduct = async () => {
	// select the first product of the list of all the products that were sorted randomly
	requests += 1;
	logger.info("GET request received at /randomproduct")
	logger.info(requests + " requests received so far")
	return (await mysql.query(`SELECT *
                              FROM products
                              ORDER BY RAND()
                              LIMIT 1;`))[0][0];
};

const queryAllProducts = async () => {
	// get all products existing in DB
	requests += 1;
	logger.info("GET request received at /products")
	logger.info(requests + " requests received so far")
	return (await mysql.query("SELECT * FROM products;"))[0];
};

const queryAllCategories = async () => {
	requests += 1;
	logger.info("GET request received at /categories")
	logger.info(requests + " requests received so far")
	return (await mysql.query("SELECT * FROM categories;"))[0];
};

const queryAllOrders = async () => {
	requests += 1;
	logger.info("GET request received at /allorders")
	logger.info(requests + " requests received so far")
	return (await mysql.query("SELECT * FROM orders;"))[0];
};

const queryOrdersByUser = async (userId) => {
	requests += 1;
	logger.info("GET request received at /orders, parameters: " + JSON.stringify(userId))
	logger.info(requests + " requests received so far")
	return (
		await mysql.query(`SELECT *
                           FROM orders
                                    INNER JOIN order_items ON orders.id = order_items.order_id
                           WHERE user_id = "${userId}"`)
	)[0]; // Not a perfect analog for NoSQL, since SQL cannot return a list.
};

const queryOrderById = async (id) => {
	requests += 1;
	logger.info("GET request received at /order/:orderId, parameters: " + JSON.stringify(id))
	logger.info(requests + " requests received so far")
	return (
		await mysql.query(`SELECT *
                           FROM orders
                           WHERE id = "${id}"`)
	)[0][0];
};

const queryUserById = async (id) => {
	requests += 1;
	logger.info("GET request received at /user/:userId, parameters: " + JSON.stringify(id))
	logger.info(requests + " requests received so far")
	return (
		await mysql.query(`SELECT *
                           FROM users
                           WHERE id = "${id}";`)
	)[0][0];
};

const queryAllUsers = async () => {
	// Select everything from users
	requests += 1;
	logger.info("GET request received at /users")
	logger.info(requests + " requests received so far")
	return (await mysql.query("SELECT * FROM users;"))[0];
};

const insertOrder = async (order) => {
	// insert values from order param into DB row
	requests += 1;
	logger.info("POST request received at /orders, parameters: " + JSON.stringify(order))
	logger.info(requests + " requests received so far")
	return (await mysql.query(`INSERT INTO orders(id, user_id, total_amount)
                                 VALUES ("${order.id}", "${order.user_id}", "${order.total_amount}");`)
	);
};

const updateUser = async (id, updates) => {
	// update provided fields for given id
	requests += 1;
	logger.info("PATCH request received at /user/:userId, parameters: " + id + " , " + JSON.stringify(updates))
	logger.info(requests + " requests received so far")
	await mysql.query(`UPDATE users
                                SET name = "${updates.name}",
                                    email = "${updates.email}",
                                    password = "${updates.password}"
                                WHERE id = "${id}";`);
	// return updated values
	return { id: id, email: updates.email }
};

module.exports = {
	init,
	queryRandomProduct,
	queryProductById,
	queryAllProducts,
	queryAllCategories,
	queryAllOrders,
	queryOrderById,
	queryOrdersByUser,
	queryUserById,
	queryAllUsers,
	insertOrder,
	updateUser,
};
