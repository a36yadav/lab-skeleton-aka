require("dotenv").config();
const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const {
  GetCommand,
  ScanCommand,
  PutCommand,
  UpdateCommand,
  DynamoDBDocumentClient,
} = require("@aws-sdk/lib-dynamodb");
const uuid = require("uuid");

let client;
let docClient;

const init = () => {
  client = new DynamoDBClient({ region: process.env.AWS_REGION });
  docClient = DynamoDBDocumentClient.from(client);
  console.log("DynamoDB connected!");
};

const queryRandomProduct = async () => {
  const command = new ScanCommand({
    TableName: "Products",
  });

  // get all products
  const response = await docClient.send(command);
  const allProducts = response.Items;

  // pick a random product from the response
  const randomProductIndex = Math.floor(Math.random() * allProducts.length);
  return allProducts[randomProductIndex];
};

const queryProductById = async (productId) => {
  const command = new GetCommand({
    TableName: "Products",
    Key: {
      id: productId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllProducts = async (category = "") => {
  // command to filter based on given category
  const command = new ScanCommand({
    TableName: "Products",
    FilterExpression: "category_id = :category_id",
    ExpressionAttributeValues: {
      ":category_id": category,
    },
  });

  // send scan command
  const response = await docClient.send(command);

  // return all items in response
  return response.Items;
};

const queryAllCategories = async () => {
  const command = new ScanCommand({
    TableName: "Categories",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryAllOrders = async () => {
  const command = new ScanCommand({
    TableName: "Orders",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrdersByUser = async (userId) => {
  const command = new ScanCommand({
    TableName: "Orders",
    FilterExpression: "user_id = :user_id",
    ExpressionAttributeValues: {
      ":user_id": userId,
    },
  });

  const response = await docClient.send(command);
  return response.Items;
};

const queryOrderById = async (userId) => {
  const command = new GetCommand({
    TableName: "Orders",
    Key: {
      id: userId,
    },
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryUserById = async (userId) => {
  const command = new GetCommand({
    TableName: "Users",
    Key: {
      id: userId,
    },
    ProjectionExpression: "id, email",
  });

  const response = await docClient.send(command);
  return response.Item;
};

const queryAllUsers = async () => {
  const command = new ScanCommand({
    TableName: "Users",
  });

  const response = await docClient.send(command);
  return response.Items;
};

const insertOrder = async (order) => {
  // item to be inserted
  const item = {
    id: order.id,
    total_amount: order.total_amount,
    user_id: order.user_id,
  };

  // corresponding put command
  const command = new PutCommand({
    TableName: "Orders",
    Item: item,
  });
  await docClient.send(command);

  // return inserted item
  return item;
};

const updateUser = async (id, updates) => {
  // data structures to help hold the update expressions and attribute values
  const updateExpression = [];
  const expressionAttributeValues = {};

  // construct the update expression + values, depending on the updates param
  Object.keys(updates).forEach((key) => {
    if (key != 'id') {
      updateExpression.push(`${key} = :${key}`);
      expressionAttributeValues[`:${key}`] = updates[key];
    }
  });

  // command with constructed update expression
  const command = new UpdateCommand({
    TableName: "Users",
    Key: {
      id: id,
    },
    UpdateExpression: `SET ${updateExpression.join(", ")}`,
    ExpressionAttributeValues: expressionAttributeValues,
    ReturnValues: "ALL_NEW", // new values should be returned
  });
  const response = await docClient.send(command);
  return response.Attributes;
};

module.exports = {
  init,
  queryRandomProduct,
  queryProductById,
  queryAllProducts,
  queryAllCategories,
  queryAllOrders,
  queryOrderById,
  queryOrdersByUser,
  queryUserById,
  queryAllUsers,
  insertOrder,
  updateUser,
};
